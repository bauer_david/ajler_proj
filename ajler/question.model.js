const mongoose = require('mongoose');
const autoIncrementModelID = require('./counter.model');

const questionSchema = new mongoose.Schema({
    id: {type: Number, unique: true, min: 1},
    question: {type: String, unique: true, required: true},
    right: {type: String, required: true},
    wrong1: {type: String},
    wrong2: {type: String},
    wrong3: {type: String},
    topic1: {type: Number},
    topic2: {type: Number},
    topic3: {type: Number}
});

questionSchema.pre('save', function (next) {
    if (!this.isNew) {
      next();
      return;
    }
  
    autoIncrementModelID('activities', this, next);
  });

mongoose.model('question', questionSchema);