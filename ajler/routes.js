const router = require('express').Router();
const passport = require('passport');
const fs = require('fs');
const mongoose = require('mongoose');
const userModel = mongoose.model('user');
const questionModel = mongoose.model('question');
const ScoreModel = mongoose.model('score');
const bcrypt = require('bcrypt');

router.route('/login').post((req, res) => {
    if(req.body.username && req.body.password) {
        // here, the method with parameters (error, user) will be recognised as the done() callback by PassportJS
        passport.authenticate('local', (error, user) => {
            if(error) {
                return res.status(403).send(error);
            } else {
                // this calls the serializeUser method and initialises the session
                req.logIn(user, (error) => {
                    if(error) return res.status(500).send(error);
                    return res.status(200).send({user});
                });
            }
        })(req, res);
    } else {
        res.status(400).send({msg: "Missing username or password"});
    }
});

router.route('/logout').post((req, res) => {
    console.log(req.session.passport.user);
    if(req.isAuthenticated()) {
        req.logout();
        res.status(200).send({msg: "Logout successful"});
    } else {
        res.status(403).send({msg: "Log in, before you log out"})
    }
});

router.route('/register').post((req, res) => {
    if(req.body.username && req.body.password && req.body.email) {
        const user = new userModel({
            username: req.body.username,
            password: req.body.password,
            email: req.body.email,
            role: 'user',
            score: 0
        });
        const userScore = new ScoreModel({
            username: req.body.username,
            muv_jo: 0,
            muv_rossz: 0,
            tori_jo: 0,
            tori_rossz: 0,
            magyar_jo: 0,
            magyar_rossz: 0,
            irodalom_jo: 0,
            irodalom_rossz: 0,
            filmek_jo: 0,
            filmek_rossz: 0,
            egyeb_jo: 0,
            egyeb_rossz: 0,
            tudom_jo: 0,
            tudom_rossz: 0,
            sport_jo: 0,
            sport_rossz: 0
        })
        userScore.save(function(error){
            if(error) return res.status(500).send(error);
        })
        user.save(function(error) {
            if(error) return res.status(500).send(error);
            return res.status(200).send({msg: 'User registered!'});
        })
    } else {
        return res.status(400).send({msg: "Username or password is missing"});
    }
});

router.route('/users').post((req, res) => {
        userModel.find({}, function(err, users) {
            if(err) return res.status(500).send(err);
            return res.status(200).send(users);
        });
})

router.route('/update-users').post((req, res) => {
        x = 0;
        talalt = 0;
        while(req.body.users[x]){
            if(req.body.users[x].changed){
                talalt = 1;
                userModel.findOneAndUpdate({username: req.body.users[x].username}, {role: req.body.users[x].role}, (err, users) =>{
                    if(err) return res.status(500).send(err);
                });
            }
            x++;

        }
        if (talalt) return res.status(200).send({msg: "Successfully updated"});
        return res.status(400).send({msg: "Update was not successful"});
})


router.route('/update-usersScore').post((req, res) => {

    userModel.findOne({username: req.body.username}, (err, users) =>{
        users["score"] = users["score"] + req.body.score;
        userModel.findOneAndUpdate({username: req.body.username}, {score: users["score"]}, (err, users) =>{
            if(err) return res.status(500).send(err)
        })
        if(err) return res.status(500).send(err)
        });
    ScoreModel.findOne({username: req.body.username}, (err, userscores) =>{
        //muv_jo = req.body.req.body.rightAnswersByTopic[0];
        if(err) return res.status(500).send(err);
        userscores["muv_jo"] = userscores["muv_jo"]+req.body.rightAnswersByTopic[0];
        userscores["tori_jo"] = userscores["tori_jo"]+req.body.rightAnswersByTopic[1];
        userscores["magyar_jo"] = userscores["magyar_jo"]+req.body.rightAnswersByTopic[2];
        userscores["irodalom_jo"] = userscores["irodalom_jo"]+req.body.rightAnswersByTopic[3];
        userscores["filmek_jo"] = userscores["filmek_jo"]+req.body.rightAnswersByTopic[4];
        userscores["egyeb_jo"] = userscores["egyeb_jo"]+req.body.rightAnswersByTopic[5];
        userscores["tudom_jo"] = userscores["tudom_jo"]+req.body.rightAnswersByTopic[6];
        userscores["sport_jo"] = userscores["sport_jo"]+req.body.rightAnswersByTopic[7];

        userscores["muv_rossz"] = userscores["muv_rossz"]+req.body.wrongAnswersByTopic[0];
        userscores["tori_rossz"] = userscores["tori_rossz"]+req.body.wrongAnswersByTopic[1];
        userscores["magyar_rossz"] = userscores["magyar_rossz"]+req.body.wrongAnswersByTopic[2];
        userscores["irodalom_rossz"] = userscores["irodalom_rossz"]+req.body.wrongAnswersByTopic[3];
        userscores["filmek_rossz"] = userscores["filmek_rossz"]+req.body.wrongAnswersByTopic[4];
        userscores["egyeb_rossz"] = userscores["egyeb_rossz"]+req.body.wrongAnswersByTopic[5];
        userscores["tudom_rossz"] = userscores["tudom_rossz"]+req.body.wrongAnswersByTopic[6];
        userscores["sport_rossz"] = userscores["sport_rossz"]+req.body.wrongAnswersByTopic[7];

        ScoreModel.findOneAndUpdate({username: req.body.username}, {muv_jo: userscores["muv_jo"],
        muv_rossz: userscores["muv_rossz"], tori_jo: userscores["tori_jo"], tori_rossz: userscores["tori_rossz"],
        magyar_jo: userscores["magyar_jo"], magyar_rossz: userscores["magyar_rossz"], 
        irodalom_jo: userscores["irodalom_jo"], irodalom_rossz: userscores["irodalom_rossz"],
        filmek_jo: userscores["filmek_jo"], filmek_rossz: userscores["filmek_rossz"],
        egyeb_jo: userscores["egyeb_jo"], egyeb_rossz: userscores["egyeb_rossz"],
        tudom_jo: userscores["tudom_jo"], tudom_rossz: userscores["tudom_rossz"],
        sport_jo: userscores["sport_jo"], sport_rossz: userscores["sport_rossz"]},
        (err, done) =>{
            if(err) return res.status(500).send(err);
        });

        return res.status(200).send({msg: "Successfully updated"});
    });


    });


router.route('/update').post((req, res) => {
    if(req.body.username){

        var newPassword; 

        bcrypt.genSalt(10, function(err, salt) {
            if(err) return next('There was an error during the salt generation');
            bcrypt.hash(req.body.password, salt, function(error, hash) {
                if(error) return next('There was en error during the hashing procedure');
                newPassword = hash;
                
                userModel.findOneAndUpdate({username: req.body.username}, {email: req.body.email, password: newPassword}, (err, users) =>{
                    if(err) return res.status(500).send(err)
                    return res.status(200).send({msg: req.body.email});
                });
            })
        })


    }else{
        return res.status(400).send({msg: "Email or password is missing"});
    }
})

router.route('/delete-user').post((req, res) => {
    userModel.findOneAndDelete({username: req.body.username}, (err, user) => {
        if(err) return res.status(500).send(err);
        return res.status(200).send({msg: "Successfully deleted"})
    })
})


router.route('/getPlace').post((req, res) => {
    userModel.find({}).sort({ score: 1}).exec(function (err, stored){
        if(err) return res.status(500).send(err);
        return res.status(200).send(stored);
    });
})


/********************************************************************************************* */

router.route('/addQuestion').post((req, res) => {
    if(req.body.question && req.body.right && req.body.wrongs && req.body.topics) {
        const question = new questionModel({
            question: req.body.question,
            right: req.body.right,
            wrong1: req.body.wrongs[0],
            wrong2: req.body.wrongs[1],
            wrong3: req.body.wrongs[2],
            topic1: req.body.topics[0],
            topic2: req.body.topics[1]?req.body.topics[1]:null,
            topic3:  req.body.topics[2]?req.body.topics[2]:null
        });

        if(req.body.topics[1]) this.topic2 = req.body.topics[1];
        if(req.body.topics[2]) this.topic3 = req.body.topics[2];
        question.save(function(error) {
            if(error) return res.status(500).send(error);
            return res.status(200).send({id: question["id"]});
        })
    } else {
        return res.status(400).send({msg: "Something is missing"});
    }
});

router.route('/getQuestions').post((req, res) => {
    questionModel.find({}, function(err, users) {
        if(err) return res.status(500).send(err);
        return res.status(200).send(users);
    });
})

router.route('/delete-question').post((req, res) => {
    questionModel.findOneAndDelete({id: req.body.id}, (err, question) => {
        if(err) return res.status(500).send(err);
        return res.status(200).send({msg: "Successfully deleted"})
    })
})


router.route('/update-question').post((req, res) => {
    
    
    questionModel.findOneAndUpdate({id: req.body.id}, {question: req.body.question, right: req.body.right, wrong1: req.body.wrongs[0],
    wrong2: req.body.wrongs[1], wrong3: req.body.wrongs[2], topic1: req.body.topics[0], topic2: req.body.topics[1]?req.body.topics[1]:null,
    topic3: req.body.topics[2]?req.body.topics[2]:null}, (err, question) =>{
        if(err) return res.status(500).send(err);
        return res.status(200).send({msg: "Successfully updated"});
    });
})



/************************************************************************************************** */

router.route('/getScore').post((req, res) => {
    ScoreModel.findOne({username: req.body.username}, (err, users) =>{
                if(err) return res.status(500).send(err);
                return res.status(200).send({users});
    });
})

module.exports = router;