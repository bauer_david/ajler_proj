const mongoose = require('mongoose');

const scoreSchema = new mongoose.Schema({
    username: {type: String, unique: true, required: true},
    muv_jo: {type: Number},
    muv_rossz: {type: Number},
    tori_jo: {type: Number},
    tori_rossz: {type: Number},
    magyar_jo: {type: Number},
    magyar_rossz: {type: Number},
    irodalom_jo: {type: Number},
    irodalom_rossz: {type: Number},
    filmek_jo: {type: Number},
    filmek_rossz: {type: Number},
    egyeb_jo: {type: Number},
    egyeb_rossz: {type: Number},
    tudom_jo: {type: Number},
    tudom_rossz: {type: Number},
    sport_jo: {type: Number},
    sport_rossz: {type: Number}
});

mongoose.model('score', scoreSchema);