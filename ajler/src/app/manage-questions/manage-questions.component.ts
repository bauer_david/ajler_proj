import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { QuestionManagerService } from '../services/question-manager.service';
import { getRangeLabelHungarian } from '../utils/paginator-extension';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SelectionModel} from '@angular/cdk/collections';
import { TOPIC_NAMES, TOPIC_IMG_PATHS } from 'src/environments/environment';
import {MatDialog} from '@angular/material/dialog';

export interface QuestionStat {
  id: number;
  question: string;
  right: string;
  wrongs: string[];
  topics: number[];
}

@Component({
  selector: 'app-manage-questions',
  templateUrl: './manage-questions.component.html',
  styleUrls: ['./manage-questions.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ManageQuestionsComponent implements OnInit {
  topicNames = TOPIC_NAMES;
  topicImagePaths = TOPIC_IMG_PATHS;

  questions: MatTableDataSource<QuestionStat>;
  columnsToDisplay: string[] = ["select", "question", "topics", "actions"];
  expandedElement: QuestionStat | null;
  tempElement: QuestionStat | null;
  selection = new SelectionModel<QuestionStat>(true, []);
  everyQuestionSelected: boolean = false;

  isAllSelected() {
    if(this.questions == undefined){
      return false;
    }
    const numSelected = this.selection.selected.length;
    const numRows = this.questions.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.questions.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: QuestionStat): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private questionManager: QuestionManagerService, private dialog: MatDialog) { }

  ngOnInit(): void {

    this.questionManager.getQuestions().subscribe(data =>{
      var x = 0;
      var qs : Array<any> = [{id: data[x]["id"], question: data[x]["question"], right: data[x]["right"],
      wrongs: [data[x]["wrong1"], data[x]["wrong2"], data[x]["wrong3"]], 
      topics: [data[x]["topic1"], data[x]["topic2"]?data[x]["topic2"]:null, data[x]["topic3"]?data[x]["topic3"]:null] }];
      
      for(x = 1; x < data.length; x++){
        qs.push({id: data[x]["id"], question: data[x]["question"], right: data[x]["right"],
        wrongs: [data[x]["wrong1"], data[x]["wrong2"], data[x]["wrong3"]], 
        topics: [data[x]["topic1"], data[x]["topic2"]?data[x]["topic2"]:null, data[x]["topic3"]?data[x]["topic3"]:null] });
      }

    this.questions = new MatTableDataSource(qs);
    this.questions.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Megjelenített sorok száma:';
    this.paginator._intl.getRangeLabel = getRangeLabelHungarian;
   })
  }

  removeQuestion(row: QuestionStat, shouldConfirm = true){
    if(shouldConfirm) {
      if(!window.confirm("Biztos, hogy törölni szeretnéd a kiválasztott elemet?")){
        return;
      }
    }
    this.questionManager.deleteQuestion(row.id).subscribe(data => {
      console.log('data', data);
      var index = this.questions.data.findIndex( obj => obj.id == row.id);
      if(this.tempElement != null && this.tempElement.id === this.questions.data[index].id){
        this.tempElement = null;
      }
      if (index > -1) {
        this.questions.data.splice(index, 1);
      }
      this.questions._updateChangeSubscription();
    }, error => {
      console.log('error', error);
    });
  }
  saveQuestion(row: QuestionStat){
    if(this.expandedElement == null){
      return;
    }
    var index = -1;
    do{
      index = row.topics.indexOf(null, 0);
      if (index > -1) {
        row.topics.splice(index, 1);
      }
    } while(index >= 0)
    console.log(row.topics);
    this.questionManager.updateQuestion(row.id, row.question, row.right, row.wrongs, row.topics).subscribe(data=> {
      var index = this.questions.data.findIndex( obj => obj.id == row.id);
      this.questions.data[index].right = this.tempElement.right = this.expandedElement.right;
      this.questions.data[index].wrongs = this.tempElement.wrongs = this.expandedElement.wrongs;
      this.questions.data[index].topics = this.tempElement.topics = this.expandedElement.topics;
      this.questions.data[index].question = this.tempElement.question = this.expandedElement.question;
      this.questions._updateChangeSubscription();
      this.expandedElement = null;
      this.tempElement = null;
    }, error =>{
      console.log('error', error);
    })
    
  }

  deleteSelected(){
    if(!window.confirm("Biztos vagy benne, hogy törölni szeretnéd a kiválasztott elemeket?")){
      return;
    }
    this.selection.selected.forEach(element => {
      this.removeQuestion(element, false);
    });
  }

  toggleTopic(row: QuestionStat, idx: number, event){
    event.stopPropagation();
    var questionIndex = this.questions.data.findIndex( obj => obj.id == row.id);
    var topicIndex = this.questions.data[questionIndex].topics.findIndex(elem => elem==idx);
    if(topicIndex > -1){
      this.questions.data[questionIndex].topics.splice(topicIndex, 1);
    } else {
      this.questions.data[questionIndex].topics.push(idx);
    }
    this.questions._updateChangeSubscription();
  }

  restoreValue(element: QuestionStat){
    if(this.tempElement != null) {
      var questionIndex = this.questions.data.findIndex( obj => obj.id == this.tempElement.id);
      this.questions.data[questionIndex].id = this.tempElement.id;
      this.questions.data[questionIndex].question = this.tempElement.question;
      this.questions.data[questionIndex].right = this.tempElement.right;
      this.questions.data[questionIndex].wrongs = [];
      this.tempElement.wrongs.forEach(element => {
        this.questions.data[questionIndex].wrongs.push(element);
      });
      this.questions.data[questionIndex].topics = [];
      this.tempElement.topics.forEach(element => {
        this.questions.data[questionIndex].topics.push(element);
      });

    }
    if(this.tempElement == null || this.tempElement.id != element.id){
      this.tempElement = {
        "id": element.id,
        "question": element.question,
        "right": element.right,
        "wrongs": [],
        "topics": []
      };
      element.wrongs.forEach(element => {
        this.tempElement.wrongs.push(element);
      });
      element.topics.forEach(element => {
        this.tempElement.topics.push(element);
      });
    } else if(this.tempElement.id == element.id){
      this.tempElement = null;
    }

  }

  trackByFn(index: any, item: any){
    return index;
  }

  openNewQuestionDialog(){
    const dialogRef = this.dialog.open(NewQuestionDialog);

    dialogRef.afterClosed().subscribe(result => {
      
      console.log(`Dialog result: ${result.question}`);
      this.questionManager.saveQuestion(result.question, result.right, result.wrongs, result.topics).subscribe(data => {
        if(data.id){
          this.questions.data.push({id: data.id, question: result.question, right: result.right, wrongs: result.wrongs, topics: result.topics});
          this.questions._updateChangeSubscription();
        }
      }, error=>{
        console.log(error);
      });
    });
  }
}

@Component({
  selector: 'new-question-dialog',
  templateUrl: 'new-question-dialog.html',
  styleUrls: ['./new-question-dialog.css',
              './manage-questions.component.css']
})
export class NewQuestionDialog {
  newQuestion: QuestionStat;
  topicNames = TOPIC_NAMES;
  topicImagePaths = TOPIC_IMG_PATHS;

  constructor(private questionManager: QuestionManagerService){}
  ngOnInit(){
      this.newQuestion = {id: 0, question: "", right: "", wrongs: ["","",""], topics: []}
  }

  toggleTopic(id: number){
    var topicIndex = this.newQuestion.topics.findIndex(elem => elem==id);
    if(topicIndex > -1){
      this.newQuestion.topics.splice(topicIndex, 1);
    } else {
      this.newQuestion.topics.push(id);
    }
  }
}
