import {MatDialog, MatDialogContent} from '@angular/material/dialog';
import { Component, OnInit} from '@angular/core';
import { QuestionStat } from './manage-questions.component';
import { TOPIC_NAMES, TOPIC_IMG_PATHS } from 'src/environments/environment';

@Component({
    selector: 'new-question-dialog',
    templateUrl: 'new-question-dialog.html',
  })
  export class NewQuestionDialog implements OnInit {
    topicNames = TOPIC_NAMES;
    topicImagePaths = TOPIC_IMG_PATHS;

  newQuestion: QuestionStat;
  ngOnInit(){
      this.newQuestion = {id: 0, question: "", right: "", wrongs: ["","",""], topics: []}
  }
}