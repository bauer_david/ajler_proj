import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ManageQuestionsComponent } from './manage-questions/manage-questions.component';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { PlayQuizComponent } from './play-quiz/play-quiz.component';
import { MainPageComponent } from './main-page/main-page.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { ErrorComponent } from './error/error.component';


const routes: Routes = [
  {
    path: '',
    component: MainPageComponent
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'logout',
    component: LoginComponent,
    canActivate: [AuthGuardService]
  },

  {
    path: 'registration',
    component: RegistrationComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'play',
    component: PlayQuizComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'manage_questions',
    component: ManageQuestionsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'manage_users',
    component: ManageUsersComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'account',
    component: AccountSettingsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'error',
    component: ErrorComponent
  }
  ,
  {
    path: '**',
    component: ErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
