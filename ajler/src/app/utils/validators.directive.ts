import { ValidatorFn, AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { Directive, Input } from '@angular/core';

export function passCheckValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const match = control.get("pwd").value === control.get("pwd2").value;
      return match ? null: {'doesnotmatch': {value: control.get("pwd2").value}};
    };
}

@Directive({
    selector: '[appPassCheck]',
    providers: [{provide: NG_VALIDATORS, useExisting: PassCheckValidatorDirective, multi: true}]
  })
  export class PassCheckValidatorDirective implements Validator {
    @Input('appPassCheck') forbiddenName: string;
  
    validate(control: AbstractControl): {[key: string]: any} | null {
      return this.forbiddenName ? passCheckValidator()(control)
                                : null;
    }
  }