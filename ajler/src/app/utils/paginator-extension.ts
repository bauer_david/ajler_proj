import { DecimalPipe } from '@angular/common';
const decimalPipe = new DecimalPipe(navigator.language);

export function getRangeLabelHungarian(page: number, pageSize: number, length: number) {
    const start = page * pageSize + 1;
    const end = Math.min((page + 1) * pageSize, length);
    let rag: string;
    if(length%1000 === 0){
      rag = "ből";
    } else if(length%100 === 0){
      rag = "ból";
    } else if(length%10 === 0){
      switch((length%10)%10){
        case 1:
        case 4:
        case 5:
        case 7:
        case 9:
          rag = "ből";
          break;
        default:
          rag = "ból";
      }
    } else {
      switch(length%10){
        case 1:
        case 2:
        case 4:
        case 5:
        case 7:
        case 9:
          rag = "ből";
          break;
        default:
          rag = "ból";
      }
    }
    return `${start} - ${end} a ${decimalPipe.transform(length)}-${rag}`;
  }