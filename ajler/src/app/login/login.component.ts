import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountManagerService } from '../services/account-manager.service';
import { ROLES } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  outputs: ['loginData']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  role: string;
  msg: string;
  @Output() loginData = new EventEmitter<any>();

  constructor(private route: ActivatedRoute, private router: Router, private loginService: AccountManagerService) { }

  ngOnInit(): void {
    localStorage.clear();
    this.route.params.subscribe(params => {
      //console.log(params);
      if(params.msg) {
        this.msg = params.msg;
      } else {
        this.msg = "";
      }
    });
  }
  onSubmit(){

    this.loginService.login(this.username, this.password, this.role).subscribe(data => {
      localStorage.setItem("username", this.username);
      localStorage.setItem("role", data.user["role"]);
      this.msg = null;
      this.loginData.emit({"username": this.username, "role": localStorage.getItem("role")});
      this.router.navigate(["/"]);
    }, error => {
      console.log('error', error);
    });
  }

}
