import {FormControl, FormGroup, Validators} from '@angular/forms';
import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountManagerService } from '../services/account-manager.service';
import { passCheckValidator } from '../utils/validators.directive';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registrationForm: FormGroup;
  _username: string;
  _password: string;
  _passCheck: string;
  _email: string;

  constructor(private route: ActivatedRoute, private router: Router, private registrationService: AccountManagerService) { }

  ngOnInit(): void {
    this.registrationForm = new FormGroup({
      'username': new FormControl(this._username, [
        Validators.minLength(6),
        Validators.maxLength(16),
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9 \-]+$')
      ]),
      'email': new FormControl(this._email, [
        Validators.required,
        Validators.email
      ]),
      'pwd': new FormControl(this._password, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(18),
        Validators.pattern('^[a-zA-Z0-9]*$')
      ]),
      'pwd2': new FormControl(this._passCheck)
    });
    this.registrationForm.validator = passCheckValidator();
  }

  get username() {return this.registrationForm.get('username');}
  get email() {return this.registrationForm.get('email');}
  get pwd() {return this.registrationForm.get('pwd');}
  get pwd2() {return this.registrationForm.get('pwd2');}
  
  onSubmit(){
    this.registrationService.register(this._username, this._password, this._email).subscribe(data => {
      //console.log('data', data);
      this.router.navigate(['/login', {msg:"Sikeres regisztráció! Jelentkezz be a folytatáshoz."}]);
    }, error => {
      console.log('error', error);
    });
  }
}
