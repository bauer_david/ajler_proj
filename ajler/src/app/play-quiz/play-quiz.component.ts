import { Component, OnInit} from '@angular/core';
import { Topic, TOPIC_IMG_PATHS, TOPIC_NAMES, SECONDS_PER_QUESTION, QUIZ_LENGTHS, LENGTH_STRINGS} from 'src/environments/environment';
import { PlayQuizService } from '../services/play-quiz.service';
import { interval, Observable, Subscription } from 'rxjs';
import { AccountManagerService } from '../services/account-manager.service';

@Component({
  selector: 'app-play-quiz',
  templateUrl: './play-quiz.component.html',
  styleUrls: ['./play-quiz.component.css']
})
export class PlayQuizComponent implements OnInit {
  quizStarted:boolean;
  selectedTopics:Array<boolean>;
  topicImagePaths = TOPIC_IMG_PATHS;
  topicNames = TOPIC_NAMES;
  secondsPerQuestion = SECONDS_PER_QUESTION;

  quizLengths = QUIZ_LENGTHS;
  quizLengthStrings = LENGTH_STRINGS;

  quizLength: number;

  questions: Array<any>;
  questionCount: number;
  questionTopics: Array<number>;
  currentQuestion: string;
  rightAnswerIndex:number;
  answers:Array<string>;
  providedAnswer:number;
  timeLeft: number;
  curSec: number;
  timerSubscription: Subscription;

  nRightAnswers: Array<number>;
  rightAnswersTotal: number;
  nWrongAnswers: Array<number>;
  wrongAnswersTotal: number;
  score: number;

  rejectMessage: string;
  constructor(private quizService: PlayQuizService, private accountManager: AccountManagerService) { }

  ngOnInit(): void {
    if(this.selectedTopics == undefined)
      this.selectedTopics = new Array<boolean>(8).fill(true);
      this.quizStarted = false;
    this.quizLength = 10;
  }

  toggleTopic(topicIndex: number){
    this.selectedTopics[topicIndex] = !this.selectedTopics[topicIndex];
  }

  startQuiz(){
    this.nRightAnswers = new Array(this.topicNames.length).fill(0);
    this.nWrongAnswers = new Array(this.topicNames.length).fill(0);
    this.questionCount = -1;
    this.score = 0;
    this.rightAnswersTotal = 0;
    this.wrongAnswersTotal = 0;
    var topicIdx = new Array<number>();
    this.selectedTopics.forEach((element, index) => {
      if(element){
        topicIdx.push(index);
      }
    });
    this.quizService.getQuiz(topicIdx, this.quizLength).then(questions => {
      this.questions = questions;
      this.nextQuestion();
      this.quizStarted=true;
      this.rejectMessage = undefined;
    }).catch(reject =>{
      this.rejectMessage = reject;
    });
  }

  getTopics(): Array<number>{
    return this.questions[this.questionCount].topics;
  }

  getQuestion():string{
    return this.questions[this.questionCount].question;
  }

  getRightAnswer(): string{
    return this.questions[this.questionCount].right;
  }

  getWrongAnswers(): Array<string>{
    return this.questions[this.questionCount].wrongs;
  }

  getSumOfRightAnswers(){
    return this.nRightAnswers.reduce((sum, current) => sum + current, 0);
  }

  getSumOfWrongAnswers(){
    return this.nWrongAnswers.reduce((sum, current) => sum + current, 0);
  }

  startTimer(){
    this.timeLeft = this.secondsPerQuestion;
    const timer$ = interval(1000);

    this.timerSubscription = timer$.subscribe((sec) => {
      this.timeLeft -=1;
      this.curSec = sec;

      if (this.curSec === this.secondsPerQuestion-1) {
        this.timerSubscription.unsubscribe();
        this.providedAnswer = -1;
      }
    });
  }

  nextQuestion(){
    this.providedAnswer = undefined;
    this.questionCount++;
    if(this.questionCount === this.questions.length){
      this.accountManager.storeResults(localStorage.getItem("username"), this.score, this.nRightAnswers, this.nWrongAnswers, this.rightAnswersTotal, this.wrongAnswersTotal).subscribe(data=>{
        console.log(data);
      });
      return;
    }
    this.questionTopics = this.getTopics();
    this.currentQuestion = this.getQuestion();
    this.answers = this.getWrongAnswers();
    var shuffleArray = require("array-shuffle");
    this.answers = shuffleArray(this.answers);
    this.rightAnswerIndex = Math.floor(Math.random()*4);
    this.answers.splice(this.rightAnswerIndex, 0, this.getRightAnswer());
    this.startTimer();
  }

  evaluate(answerIdx: number){
    this.timerSubscription.unsubscribe();
    this.providedAnswer = answerIdx;
    if(answerIdx === this.rightAnswerIndex){
      this.questionTopics.forEach(topic => {
        this.nRightAnswers[topic]+=1;
      });
      this.rightAnswersTotal++;
      this.score+= this.timeLeft*100.0/this.secondsPerQuestion/this.questions.length;
    } else {
      this.questionTopics.forEach(topic => {
        this.nWrongAnswers[topic]+=1;
      });
      this.wrongAnswersTotal++;
    }
  }

  getButtonColor(answerIdx: number){
    if(this.providedAnswer == null){
      return 'lightgray';
    }
    if(answerIdx === this.rightAnswerIndex){
      return 'green';
    } if(answerIdx ===this.providedAnswer){
      return 'red';
    }
  }

  newQuiz(){
    this.quizStarted = false;
  }

  
}
