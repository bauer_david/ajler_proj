import { Component, OnInit, ViewChild } from '@angular/core';
import {ROLE_NAMES, ROLES} from 'src/environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { AccountManagerService } from '../services/account-manager.service';
import { MatPaginator } from '@angular/material/paginator';
import { getRangeLabelHungarian } from '../utils/paginator-extension';
import { element } from 'protractor';

export interface UserStat{
  username: string;
  email: string;
  role: string;
  changed:boolean;
}

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit {
  roleNames = ROLE_NAMES;
  roles = ROLES;
  users: MatTableDataSource<UserStat>;
  columnsToDisplay: string[] = ["username", "email", "role", "actions"];
  constructor(private manager: AccountManagerService) { }
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {

    this.manager.getUsers().subscribe(data=>{
      var x = 0;
      var users : Array<any> = [{username: data[x]["username"], email: data[x]["email"], role: data[x]["role"]}];

      for(x = 1; x < data.length; x++){
        users.push({username: data[x]["username"], email: data[x]["email"], role: data[x]["role"]});

      }

      users.forEach((element, index, arr) => {
        arr[index].changed = false;
        });
        this.users = new MatTableDataSource(users);
        this.users.paginator = this.paginator;
        //console.log(this.users.paginator);
        this.paginator._intl.itemsPerPageLabel = 'Megjelenített sorok száma:';
        this.paginator._intl.getRangeLabel = getRangeLabelHungarian;
    });

  }

  saveChanges(){
    console.log(this.users.data.filter(element => element.changed));
    var filtered = this.users.data.filter(element => element.changed);
    if(filtered.length == 0){
      return;
    }
    this.manager.updateUsers(filtered).subscribe(data=> {
      console.log("success", data);
    }, error => {
      console.log("error", error);
    });
  }

  removeUser(i: number){
    this.manager.deleteUser(this.users.data[i].username).subscribe(data=> {
      this.users.data.splice(i, 1);
      this.users._updateChangeSubscription();
      console.log(data);
    }, error => {
      console.log("error", error);
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.users.filter = filterValue.trim().toLowerCase();
  }
}
