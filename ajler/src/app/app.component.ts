import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  role: string;
  username: string;
  constructor(private router: Router){}
  ngOnInit(): void {
    this.role = localStorage.getItem("role");
    this.username = localStorage.getItem("username");
    console.log(this.username, this.role);
  }

  
  logout(){
    localStorage.clear();
    this.username = undefined;
    this.role = null;

    this.router.navigate(["/login", {msg: "Sikeres kijelentkezés!"}]);

  }

  onActivate(event){
    console.log(event);
    if(event.loginData == undefined){
      return;
    }
    event.loginData.subscribe(data => {
      this.username = data.username;
      this.role = data.role;
    })
  }

}
