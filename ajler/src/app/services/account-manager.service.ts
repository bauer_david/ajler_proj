import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { JsonPipe } from '@angular/common';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccountManagerService {

  constructor(private http: HttpClient) { }

  register(username: string, password: string, email: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/register', {username: username, password: password, email: email}, httpOptions);
  }

  update(username:string, password: string, email: string, role: string): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/update', {username: username, password: password, email: email, role: role}, httpOptions);
  }

  login(username: string, password: string, role: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/login', {username: username, password: password, role: role}, httpOptions);
  }

  logout():Observable<any>{
    return this.http.post('http://localhost:3000/logout', {});
  }

  deleteUser(username: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/delete-user', {username: username}, httpOptions);
  }

  updateUsers(users: import("src/app/manage-users/manage-users.component").UserStat[]){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/update-users', {users: users}, httpOptions);
  }

  storeResults(username: string, score: number, rightAnswersByTopic: number[], wrongAnswersByTopic: number[], rightAnswersTotal: number, wrongAnswersTotal: number){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/update-usersScore',
      {username: username,
        score: score,
        rightAnswersByTopic: rightAnswersByTopic,
        wrongAnswersByTopic: wrongAnswersByTopic,
        rightAnswersTotal: rightAnswersTotal,
        wrongAnswersTotal: wrongAnswersTotal}, httpOptions);
  }
  getAnsweredQuestions(username: string):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/getScore', {username: username}, httpOptions);
}

  getUsers(): Observable<any>{
   const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
      return this.http.post('http://localhost:3000/users', {}, httpOptions);

    }
}
