import { Injectable } from '@angular/core';
import { Observable, queueScheduler } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { stringify } from 'querystring';
import { NONE_TYPE } from '@angular/compiler';
import { callbackify } from 'util';

@Injectable({
  providedIn: 'root'
})
export class QuestionManagerService {

  constructor(private http: HttpClient) { }


  getQuestions(): Observable<any>{
    const httpOptions = {
         headers: new HttpHeaders({
           'Content-Type': 'application/json'
         })
       };
       return this.http.post('http://localhost:3000/getQuestions', {}, httpOptions);
 
     }

  deleteQuestion(id: number):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/delete-question', {id: id}, httpOptions);
  }

  updateQuestion(id: number, question:string, right:string, wrongs: string[], topics: number[]): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/update-question', {id: id, question: question, right: right, wrongs: wrongs, topics: topics}, httpOptions);
  }

  saveQuestion(question:string, right:string, wrongs: string[], topics: number[]): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('http://localhost:3000/addQuestion', {question: question, right: right, wrongs: wrongs, topics: topics}, httpOptions);
  }
}
