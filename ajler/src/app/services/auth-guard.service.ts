import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const role = localStorage.getItem("role");
    console.log(["user", "admin"].includes(role));
    if(["play", "account"].includes(route.routeConfig.path)){
      if(["user", "admin"].includes(role)){
        return true;
      }
      else {
        this.router.navigate(['/login', {msg: "Ehhez előbb be kell jelentkezned!"}]);
        return false;
      }
    }
    if(["manage_users", "manage_questions"].includes(route.routeConfig.path)){
      if(role === "admin")
        return true;
      else{
        this.router.navigate(['/error', {type: "missing_page"}]);
        return false;
      }
    }
    if(["login", "registration"].includes(route.routeConfig.path) &&
        ["user", "admin"].includes(role)){
          this.router.navigate(["/"]);
        }
    return true;
  }
}
