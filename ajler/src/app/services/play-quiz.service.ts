import { Injectable, OnInit } from '@angular/core';
import { QuestionManagerService } from '../services/question-manager.service';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { timeout, delay } from 'rxjs/operators';
import { async } from '@angular/core/testing';
import { QuestionStat } from '../manage-questions/manage-questions.component';
import { Timestamp } from 'rxjs/internal/operators/timestamp';
import { ThrowStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})

export class PlayQuizService implements OnInit {
  questions: Array<QuestionStat>;

  constructor(private questionManager: QuestionManagerService) { }
  
  ngOnInit(): void {
  }

/*
  questions = [{"id":1, "question": "0Kinek a nevéhez fűződik a dodekafónia?",
  "right": "Schönberg", "wrongs":["Bramhs", "Debussy", "Liszt Ferenc"], "topics": [0]},
  {"id":2, "question": "3Kinek a nevéhez fűződik a dodekafónia?",
  "right": "Schönberg", "wrongs":["Bramhs", "Debussy", "Liszt Ferenc"], "topics": [3]},
  {"id":3, "question": "12Kinek a nevéhez fűződik a dodekafónia?",
  "right": "Schönberg", "wrongs":["Bramhs", "Debussy", "Liszt Ferenc"], "topics": [1,2]},
  {"id":4, "question": "5Kinek a nevéhez fűződik a dodekafónia?",
  "right": "Schönberg", "wrongs":["Bramhs", "Debussy", "Liszt Ferenc"], "topics": [5]},
  {"id":5, "question": "48Kinek a nevéhez fűződik a dodekafónia?",
  "right": "Schönberg", "wrongs":["Bramhs", "Debussy", "Liszt Ferenc"], "topics": [4,7]},
 ];*/
  
 //constructor() { }

  getQuiz(topics: Array<number>, nQuestions: number): Promise<Array<QuestionStat>>{
    return new Promise((resolve, reject)=>{
      this.questionManager.getQuestions().subscribe(data =>{
        var x = 0;
        var qs : Array<QuestionStat> = [{id: data[x]["id"], question: data[x]["question"], right: data[x]["right"],
        wrongs: [data[x]["wrong1"], data[x]["wrong2"], data[x]["wrong3"]], 
        topics: [data[x]["topic1"], data[x]["topic2"]?data[x]["topic2"]:null, data[x]["topic3"]?data[x]["topic3"]:null] }];
        x++;

        for(x = 1; x < data.length; x++){
          qs.push({id: data[x]["id"], question: data[x]["question"], right: data[x]["right"],
          wrongs: [data[x]["wrong1"], data[x]["wrong2"], data[x]["wrong3"]], 
          topics: [data[x]["topic1"], data[x]["topic2"]?data[x]["topic2"]:null, data[x]["topic3"]?data[x]["topic3"]:null] });
          var questions = new Array<any>();
          topics.forEach(topic => {
            questions = questions.concat(qs.filter((element: { topics: number[]; id: any; }) => element.topics.includes(topic) && !questions.map(q => q.id).includes(element.id)));
          });
        }
        var shuffleArray = require("array-shuffle")
        this.questions = shuffleArray(questions);
        if(this.questions.length >= nQuestions){
          resolve(this.questions.slice(0, nQuestions));
        } else {
          reject("A kiválasztott témakörökben nincs elég kérdés!");
        }
        
      });
    })
  }
}


