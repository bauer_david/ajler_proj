import { Component, OnInit, ViewChild } from '@angular/core';
import { AccountManagerService } from '../services/account-manager.service';
import {MatPaginator} from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { getRangeLabelHungarian } from '../utils/paginator-extension';

export interface PlayerStat{
  name: string;
  score: number;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  dashboard: MatTableDataSource<PlayerStat>;
  columnsToDisplay: string[] = ["index", "name", "score"];
  constructor(private manager: AccountManagerService) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
  ngOnInit(): void {
    this.manager.getUsers().subscribe(data =>{
      var x = 0;
      var users : Array<any> = [{name: data[x]["username"], score: data[x]["score"]}];
      
      for(x = 1; x < data.length; x++){
        users.push({name: data[x]["username"], score: data[x]["score"]});
      }

      users = users.sort((a,b)=>b.score-a.score);
      this.dashboard = new MatTableDataSource(users);
      this.dashboard.paginator = this.paginator;
      this.paginator._intl.itemsPerPageLabel = 'Megjelenített sorok száma:';
      this.paginator._intl.getRangeLabel = getRangeLabelHungarian;

    })



  }
}
