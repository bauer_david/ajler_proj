import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  error_msg: string;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      console.log(params);
      if(params.type === "missing_page") {
        this.error_msg = "A kért oldal nem található. Kérjük, ellenőrizze az elérési útvonalat.";
      } else {
        this.error_msg = "Egy váratlan hiba miatt nem tudtuk megjeleníteni a kért oldalt.";
      }
    })
  }

}
