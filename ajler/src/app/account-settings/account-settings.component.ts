import { Component, OnInit } from '@angular/core';
import { AccountManagerService } from '../services/account-manager.service';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { passCheckValidator } from '../utils/validators.directive';

export interface AnswersPerTopic {
  topic: string;
  right_answers: number;
  wrong_answers: number;
}

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.css']
})
export class AccountSettingsComponent implements OnInit {
  username: string;
  place: number;
  answersPerTopic: AnswersPerTopic[];
  sumOfRightAnswers: number;
  sumOfWrongAnswers: number;
  
  columnsToDisplay: string[] = ["topic", "right_answers", "wrong_answers"];
  showStatistics: boolean;

  updateForm: FormGroup;
  email_address: string;
  password: string;
  passCheck:string;
  saveMsg: string;
  constructor(private router: Router, private accountManager: AccountManagerService) { }

  ngOnInit(): void {
    this.updateForm = new FormGroup({
      'email': new FormControl(this.email_address, [
        Validators.email,
      ]),
      'pwd': new FormControl(this.password, [
        Validators.minLength(8),
        Validators.maxLength(18),
        Validators.pattern('^[a-zA-Z0-9]*[a-z][a-zA-Z0-9]*[A-Z][a-zA-Z0-9]*[0-9][a-zA-Z0-9]*$')
      ]),
      'pwd2': new FormControl(this.passCheck)
    });
    this.updateForm.validator = passCheckValidator();
    this.username = localStorage.getItem("username");
    this.place;
    this.accountManager.getUsers().subscribe(data=>{
      var x = 0;
      var users : Array<any> = [{username: data[x]["username"], score: data[x]["score"]}];

      for(x = 1; x < data.length; x++){
        users.push({username: data[x]["username"], score: data[x]["score"]});
      }
      //console.log(users);

      for(var i = users.length-1; 0<i; --i){
        for(var j = 0; j < i; ++j){
          if (users[j].score < users[j+1].score){
            var c = users[j];
            users[j] = users[i];
            users[i] = c; 
          }
        }
      }
      for(var i = 0; i < users.length; i++){
        if (users[i].username === this.username) this.place = i+1;
      }

    });

    var users = new Array<any>();
    this.fgv(users);
    this.showStatistics = false;
  }

  fgv(users: Array<any>){
        
    this.accountManager.getAnsweredQuestions(this.username).subscribe(data=>{
      var x = 0;
      users = [{topic:  "Művészet", right_answers: data.users["muv_jo"], wrong_answers: data.users["muv_rossz"]}, 
      {topic: "Történelem", right_answers: data.users["tori_jo"], wrong_answers: data.users["tori_rossz"]},
      {topic: "Magyarország", right_answers: data.users["magyar_jo"], wrong_answers: data.users["magyar_rossz"]},
      {topic: "Irodalom", right_answers: data.users["irodalom_jo"], wrong_answers: data.users["irodalom_rossz"]},
      {topic: "Filmek", right_answers: data.users["filmek_jo"], wrong_answers: data.users["filmek_rossz"]},
      {topic: "Egyéb", right_answers: data.users["egyeb_jo"], wrong_answers: data.users["egyeb_rossz"]},
      {topic: "Tudomány", right_answers: data.users["tudom_jo"], wrong_answers: data.users["tudom_rossz"]},
      {topic: "Sport", right_answers: data.users["sport_jo"], wrong_answers: data.users["sport_rossz"]}
    ];
      this.answersPerTopic = users;
      this.answersPerTopic = users;
      this.sumOfRightAnswers = this.getSumOfRightAnswers();
      this.sumOfWrongAnswers = this.getSumOfWrongAnswers();
    });

  }


  get email() {return this.updateForm.get('email');}
  get pwd() {return this.updateForm.get('pwd');}
  get pwd2() {return this.updateForm.get('pwd2')}

  getSumOfRightAnswers():number{
    var sum: number = 0;
    this.answersPerTopic.forEach(element => {
      sum+=element.right_answers;
    });
    return sum;
  }

  getSumOfWrongAnswers():number{
    var sum: number = 0;
    this.answersPerTopic.forEach(element => {
      sum+=element.wrong_answers;
    });
    return sum;
  }

  onSubmit(){
    if(!this.updateForm.valid){
      this.saveMsg = "Először javítsd ki a hibákat!"
    } else if(this.email.value === null && this.pwd.value === null){
      this.saveMsg = null;
    } else {
      this.accountManager.update(this.username, this.pwd.value, this.email.value, localStorage.getItem("role")).subscribe(data => {
        console.log('data', data);
      }, error => {
        console.log('error', error);
      });
      this.saveMsg = "Sikeres frissítés";
    }
  }
}
