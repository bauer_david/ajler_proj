// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export enum Topic {
  Arts,
  History,
  Hungary,
  Literature,
  Movies,
  Other,
  Science,
  Sport
}

export const TOPIC_NAMES: Array<string> = [
  "Művészet",
  "Történelem",
  "Magyarország",
  "Irodalom",
  "Filmek",
  "Egyéb",
  "Tudomány",
  "Sport"
]

export const TOPIC_IMG_PATHS: Array<string> = [
  "assets/images/arts.png",
  "assets/images/history.png",
  "assets/images/hungary.png",
  "assets/images/literature.png",
  "assets/images/movies.png",
  "assets/images/others.png",
  "assets/images/science.png",
  "assets/images/sport.png",
]

export const SECONDS_PER_QUESTION = 25;

export const ROLE_NAMES = {
  "user": "Felhasználó",
  "admin": "Admin"
}

export const ROLES = ["user", "admin"];

export const LENGTH_STRINGS = ["Rövid", "Normál", "Hosszú"];
export const QUIZ_LENGTHS = [5, 10, 20];
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
